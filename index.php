<?php require_once "header.php"; ?>

<div id="slider">
  <a href="#" class="seta-esq"></a>
  <a href="#" class="seta-dir"></a>

  <div class="indicadores">
    <a href="#"></a>
    <a href="#" class="atual"></a>
    <a href="#"></a>
  </div> <!-- .indicadores -->

</div> <!-- #slider -->

<div id="chamadas" class="container">

  <div class="row">

    <div class="item col-lg-4 text-center">
      <img src="images/mulher1.png" alt="" />
      <h2>Especial Noivas</h2>
      <p>
        Lorem ipsum dolor sit amet.
      </p>
      <a href="#">Saiba mais</a>
    </div> <!-- .item -->

    <div class="item col-lg-4 text-center">
      <img src="images/mulher2.png" alt="" />
      <h2>Especial Formandos</h2>
      <p>
        Lorem ipsum dolor sit amet.
      </p>
      <a href="#">Saiba mais</a>
    </div> <!-- .item -->

    <div class="item col-lg-4 text-center">
      <img src="images/mulher3.png" alt="" />
      <h2>Especial Debutantes</h2>
      <p>
        Lorem ipsum dolor sit amet.
      </p>
      <a href="#">Saiba mais</a>
    </div> <!-- .item -->

  </div><!-- .row -->

</div> <!-- #chamadas -->

<div id="nossos-produtos">

  <div class="container">
  <div class="row">

  <div class="pull-left col-lg-10">
    <h1>Nossos Produtos</h1>

    <p>
      Lorem ipsum dolor sit amet.
    </p>
  </div>

  <div class="pull-right col-lg-2">
    <a href="#" class="btn-mais-premio pull-right">Mais prêmios</a>
  </div>

</div> <!-- .row -->

<div class="row">

  <div class="col-lg-4">
    <div class="item text-center">
      <img src="http://placehold.it/250x250" alt="" />
      <p>Essenza Profissional Kit Creme</p>
      <h2>R$7,98</h2>
      <div class="estrelas">
        <div class="estrela-dourada"></div>
        <div class="estrela-dourada"></div>
        <div class="estrela-dourada"></div>
        <div class="estrela-dourada"></div>
        <div class="estrela"></div>
      </div>
      <a href="#">+ Detalhes</a>
    </div> <!-- .item -->
  </div>

  <div class="col-lg-4">
    <div class="item text-center">
      <img src="http://placehold.it/250x250" alt="" />
      <p>Essenza Profissional Kit Creme</p>
      <h2>R$7,98</h2>
      <div class="estrelas">
        <div class="estrela-dourada"></div>
        <div class="estrela-dourada"></div>
        <div class="estrela-dourada"></div>
        <div class="estrela-dourada"></div>
        <div class="estrela"></div>
      </div>
      <a href="#">+ Detalhes</a>
    </div> <!-- .item -->
  </div>

  <div class="col-lg-4">
    <div class="item text-center">
      <img src="http://placehold.it/250x250" alt="" />
      <p>Essenza Profissional Kit Creme</p>
      <h2>R$7,98</h2>
      <div class="estrelas">
        <div class="estrela-dourada"></div>
        <div class="estrela-dourada"></div>
        <div class="estrela-dourada"></div>
        <div class="estrela-dourada"></div>
        <div class="estrela"></div>
      </div>
      <a href="#">+ Detalhes</a>
    </div> <!-- .item -->
  </div>

</div>

</div> <!-- .container -->

</div> <!--#nossos-produtos -->

<div id="ultimos-informativos">

  <span class="bg"></span>

  <div class="container" style="z-index: 2; position: relative;">
    <div class="row">

        <div class="pull-left col-lg-9">
          <h1>Últimos Informativos</h1>

        </div>

        <div class="pull-right col-lg-3">
          <a href="#" class="btn-mais-informativos pull-right">Mais informativos</a>
        </div>

    </div> <!-- .row -->
    <div class="row">

      <div class="col-lg-4">
        <div class="item">
          <img src="images/informativo1.png" alt="" />
          <h2>Lorem ipsum dolor sit amet</h2>
          <a href="#">+ Detalhes</a>
        </div>
      </div>

        <div class="col-lg-4">
          <div class="item">
            <img src="images/informativo2.png" alt="" />
            <h2>Lorem ipsum dolor sit amet</h2>
            <a href="#">+ Detalhes</a>
          </div>
        </div>

          <div class="col-lg-4">
            <div class="item">
              <img src="images/informativo3.png" alt="" />
              <h2>Lorem ipsum dolor sit amet</h2>
              <a href="#">+ Detalhes</a>
            </div>
          </div>

    </div>
  </div> <!-- .container -->

</div> <!-- #ultimos-informativos -->

<?php require_once "footer.php"; ?>
