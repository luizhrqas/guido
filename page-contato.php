<?php require_once "header.php"; ?>

<div class="pagetitle">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <h1>Contato</h1>
      </div>
    </div>
  </div>
</div>

<div class="pagewrapper">
  <div class="container">
    <div class="row">
      <div class="col-xs-4">
              <h2 class="pagetitle-with-border">Nossos Contatos</h2>

              <p class="icone-contato">
              <i class="fa fa-phone icone"></i>
              <strong>(73) 0000 0000</strong>
              </p>

              <p class="icone-contato">
              <i class="fa fa-envelope icone"></i>
              <strong>jocenando@jocenando.com.br</strong>
              </p>

              <p class="icone-contato">
              <i class="fa fa-map-marker icone" style="display: inline-block;"></i>
              <strong style="display: inline-block;">Av. Rio Branco, 661 - Centro Jequé - BH</strong>
              </p>
      </div>
        <div class="col-xs-8">

          <form class="form-contato" action="index.html" method="post">
            <div class="form-group">
              <input type="text" name="name" class="form-control" placeholder="Nome" value="">
            </div>
              <div class="form-group">
                <input type="text" name="name" class="form-control" placeholder="E-mail" value="">
              </div>
                <div class="form-group">
                  <input type="text" name="name" class="form-control" placeholder="Assunto" value="">
                </div>
                  <div class="form-group">
                    <textarea name="mensagem" class="form-control" rows="8" placeholder="Mensagem"></textarea>
                  </div>
                    <div class="form-group">
                      <button type="submit" name="button">Enviar</button>
                    </div>
          </form>
        </div>
    </div>
  </div>
</div>
<?php require_once "footer.php"; ?>
