<?php require_once "header.php"; ?>

<div class="pagetitle">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <h1>Produtos</h1>
      </div>
    </div>
  </div>
</div>

<div class="pagewrapper">
  <div class="container">
    <div class="row">

      <div class="col-xs-12">

        <div class="migalhas">
          <p class="pull-left">
            Exibindo: <a href="#">Lanza Healing Hair Care</a> <span>></span> <a href="#">Healing Moisture</a>  <span>></span> <a href="#">Descrição do Produto</a>
          </p>

          <a href="#" class="pull-right btn-voltar"><i class="fa fa-arrow-left"></i> Voltar</a>
        </div>

        <div class="clearfix"></div>

        <div class="row">

        <div class="col-lg-12">

            <div class="item item-produto text-center" style="padding-bottom: 0px;">

              <div class="row">

                                  <div class="col-lg-6">
                                  <div class=" esq-produto">

                  <img src="http://placehold.it/300x300" alt="" />
                </div>
                </div>

                  <div class="col-lg-6">
                  <div class=" dir-produto">

                    <h2 class="pagetitle-with-border">Californiana / Balayagem</h2>

                    <div class="estrelas">
                      <div class="estrela-dourada"></div>
                      <div class="estrela-dourada"></div>
                      <div class="estrela-dourada"></div>
                      <div class="estrela-dourada"></div>
                      <div class="estrela-dourada"></div>
                    </div>
                    <h2 class="price">R$ 274,00</h2>
                  </div>
                  </div>
              </div>
            </div> <!-- .item item-produto -->

        </div>

        </div>

        <div class="row">

          <div class="col-lg-12">
            <h2 class="pagetitle-with-border" style="margin-top:40px;">DESCRIÇÃO</h2>

                        <p>
                          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum aliquam metus dui, at convallis metus gravida nec. Fusce sollicitudin dignissim quam id aliquet. Suspendisse laoreet pulvinar tellus nec commodo. Maecenas sollicitudin arcu ac volutpat accumsan. Mauris luctus nunc dignissim dolor pulvinar dapibus. Phasellus et imperdiet massa. Interdum et malesuada fames ac ante ipsum primis in faucibus. Morbi tincidunt nisi neque. Nulla mattis massa a velit cursus, eleifend gravida dui mattis. Curabitur fermentum volutpat laoreet. Duis euismod blandit ipsum non pretium. Nullam ornare nisl hendrerit tortor suscipit vestibulum. Maecenas rutrum eget augue nec finibus. Proin maximus gravida hendrerit. In ac lorem ut felis gravida tristique.
                        </p>
                                    <p><strong class="strong-label">Contém:</strong>
                                      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum aliquam metus dui, at convallis metus gravida nec. Fusce sollicitudin dignissim quam id aliquet. Suspendisse laoreet pulvinar tellus nec commodo. Maecenas sollicitudin arcu ac volutpat accumsan. Mauris luctus nunc dignissim dolor pulvinar dapibus. Phasellus et imperdiet massa. Interdum et malesuada fames ac ante ipsum primis in faucibus. Morbi tincidunt nisi neque. Nulla mattis massa a velit cursus, eleifend gravida dui mattis. Curabitur fermentum volutpat laoreet. Duis euismod blandit ipsum non pretium. Nullam ornare nisl hendrerit tortor suscipit vestibulum. Maecenas rutrum eget augue nec finibus. Proin maximus gravida hendrerit. In ac lorem ut felis gravida tristique.
                                    </p>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>

<div id="nossos-produtos">

  <div class="container">
  <div class="row">

  <div class="pull-left col-lg-10">
    <h1>Outros Produtos</h1>

    <p>
      Lorem ipsum dolor sit amet.
    </p>
  </div>

  <div class="pull-right col-lg-2">
    <a href="#" class="btn-mais-premio pull-right">Mais produtos</a>
  </div>

</div> <!-- .row -->

<div class="row">

  <div class="col-lg-4">
    <div class="item text-center">
      <img src="http://placehold.it/250x250" alt="" />
      <p>Essenza Profissional Kit Creme</p>
      <h2>R$7,98</h2>
      <div class="estrelas">
        <div class="estrela-dourada"></div>
        <div class="estrela-dourada"></div>
        <div class="estrela-dourada"></div>
        <div class="estrela-dourada"></div>
        <div class="estrela"></div>
      </div>
      <a href="#">+ Detalhes</a>
    </div> <!-- .item -->
  </div>

  <div class="col-lg-4">
    <div class="item text-center">
      <img src="http://placehold.it/250x250" alt="" />
      <p>Essenza Profissional Kit Creme</p>
      <h2>R$7,98</h2>
      <div class="estrelas">
        <div class="estrela-dourada"></div>
        <div class="estrela-dourada"></div>
        <div class="estrela-dourada"></div>
        <div class="estrela-dourada"></div>
        <div class="estrela"></div>
      </div>
      <a href="#">+ Detalhes</a>
    </div> <!-- .item -->
  </div>

  <div class="col-lg-4">
    <div class="item text-center">
      <img src="http://placehold.it/250x250" alt="" />
      <p>Essenza Profissional Kit Creme</p>
      <h2>R$7,98</h2>
      <div class="estrelas">
        <div class="estrela-dourada"></div>
        <div class="estrela-dourada"></div>
        <div class="estrela-dourada"></div>
        <div class="estrela-dourada"></div>
        <div class="estrela"></div>
      </div>
      <a href="#">+ Detalhes</a>
    </div> <!-- .item -->
  </div>

</div>

</div> <!-- .container -->

</div> <!--#nossos-produtos -->

<?php require_once "footer.php"; ?>
