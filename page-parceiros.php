<?php require_once "header.php"; ?>

<div class="pagetitle">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <h1>Parceiros</h1>
      </div>
    </div>
  </div>
</div>

<div class="pagewrapper">
  <div class="container">
    <div class="row">
    <div class="parceiros-wrapper">

      <div class="col-lg-4">
        <div class="item">
          <img src="http://placehold.it/300x250" class="img-responsive" alt="" />

          <div class="item-infos">

                          <p>
                          <i class="fa fa-phone icone"></i>
                          <strong>48 42134124 423142134123</strong>
                          </p>

                          <p>
                          <i class="fa fa-envelope icone"></i>
                          <strong>48 42134124 423142134123</strong>
                          </p>

                          <p>
                          <i class="fa fa-globe icone"></i>
                          <strong>48 42134124 423142134123</strong>
                          </p>

                          <p>
                          <i class="fa fa-map-marker icone"></i>
                          <strong>48 42134124 423142134123</strong>
                          </p>

          </div>

        </div>
      </div>

        <div class="col-lg-4">
          <div class="item">
            <img src="http://placehold.it/300x250" class="img-responsive" alt="" />

            <div class="item-infos">

              <p>
              <i class="fa fa-phone icone"></i>
              <strong>48 42134124 423142134123</strong>
              </p>

              <p>
              <i class="fa fa-envelope icone"></i>
              <strong>48 42134124 423142134123</strong>
              </p>

              <p>
              <i class="fa fa-globe icone"></i>
              <strong>48 42134124 423142134123</strong>
              </p>

              <p>
              <i class="fa fa-map-marker icone"></i>
              <strong>48 42134124 423142134123</strong>
              </p>

            </div>

          </div>
        </div>

          <div class="col-lg-4">
            <div class="item">
              <img src="http://placehold.it/300x250" class="img-responsive" alt="" />

              <div class="item-infos">

                              <p>
                              <i class="fa fa-phone icone"></i>
                              <strong>48 42134124 423142134123</strong>
                              </p>

                              <p>
                              <i class="fa fa-envelope icone"></i>
                              <strong>48 42134124 423142134123</strong>
                              </p>

                              <p>
                              <i class="fa fa-globe icone"></i>
                              <strong>48 42134124 423142134123</strong>
                              </p>

                              <p>
                              <i class="fa fa-map-marker icone"></i>
                              <strong>48 42134124 423142134123</strong>
                              </p>

              </div>

            </div>
          </div>

    </div>
    </div>
  </div>
</div>
<?php require_once "footer.php"; ?>
