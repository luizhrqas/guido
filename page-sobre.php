<?php require_once "header.php"; ?>

<div class="pagetitle">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <h1>Quem Somos</h1>
      </div>
    </div>
  </div>
</div>

<div class="pagewrapper">
  <div class="container">
    <div class="row">
      <div class="col-xs-8">
        <h2>Lorem ipsum dolor sit amet</h2>

        <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi pharetra tellus non neque consectetur, sed semper lorem condimentum. Morbi ac nunc ac ipsum pharetra tristique pulvinar suscipit risus. In in tempor ex, mattis maximus felis. Cras at ligula ex. Vivamus augue diam, vestibulum ut massa ut, volutpat condimentum ligula. Integer accumsan dolor eu consectetur varius. Cras maximus massa ut purus fermentum, sed ultrices sapien aliquet. Vestibulum mollis pulvinar porttitor. Morbi at dolor justo. Morbi dictum ut justo eu sodales. Aenean rhoncus lobortis urna, luctus pellentesque eros venenatis non. Curabitur vitae metus diam. Integer dignissim interdum nunc, vel dapibus nulla. Mauris rutrum a tortor eget faucibus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Maecenas nec turpis diam.
        </p>
        <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi pharetra tellus non neque consectetur, sed semper lorem condimentum. Morbi ac nunc ac ipsum pharetra tristique pulvinar suscipit risus. In in tempor ex, mattis maximus felis. Cras at ligula ex. Vivamus augue diam, vestibulum ut massa ut, volutpat condimentum ligula. Integer accumsan dolor eu consectetur varius. Cras maximus massa ut purus fermentum, sed ultrices sapien aliquet. Vestibulum mollis pulvinar porttitor. Morbi at dolor justo. Morbi dictum ut justo eu sodales. Aenean rhoncus lobortis urna, luctus pellentesque eros venenatis non. Curabitur vitae metus diam. Integer dignissim interdum nunc, vel dapibus nulla. Mauris rutrum a tortor eget faucibus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Maecenas nec turpis diam.
        </p>
        <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi pharetra tellus non neque consectetur, sed semper lorem condimentum. Morbi ac nunc ac ipsum pharetra tristique pulvinar suscipit risus. In in tempor ex, mattis maximus felis. Cras at ligula ex. Vivamus augue diam, vestibulum ut massa ut, volutpat condimentum ligula. Integer accumsan dolor eu consectetur varius. Cras maximus massa ut purus fermentum, sed ultrices sapien aliquet. Vestibulum mollis pulvinar porttitor. Morbi at dolor justo. Morbi dictum ut justo eu sodales. Aenean rhoncus lobortis urna, luctus pellentesque eros venenatis non. Curabitur vitae metus diam. Integer dignissim interdum nunc, vel dapibus nulla. Mauris rutrum a tortor eget faucibus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Maecenas nec turpis diam.
        </p>
      </div>
      <div class="col-xs-4">
        <img src="images/quem-somos.gif" alt="" />
        <img src="images/mensagem.gif" alt="" />
      </div>
    </div>
  </div>
</div>

<div id="nossos-produtos" class="nossa-equipe-wrapper">
  <a href="#" class="seta-esq"></a>
  <a href="#" class="seta-dir"></a>

  <div class="container">
  <div class="row">

  <div class="pull-left col-lg-10">
    <h1>Nossa Equipe</h1>

    <p>
      Conheça um pouco o ambiente que preparamos pra você.
    </p>
  </div>

</div> <!-- .row -->

<div class="row">

  <div class="col-lg-4">
    <div class="item text-center">
      <img src="http://placehold.it/250x250" alt="" />
      <h2>Alessandra Ferreira</h2>

      <p class="infos">
        Aqui é onde vai ficar o cargo ou função profissional.
      </p>
    </div> <!-- .item -->
  </div>

  <div class="col-lg-4">
  <div class="item text-center">
    <img src="http://placehold.it/250x250" alt="" />
    <h2>Alessandra Ferreira</h2>

    <p class="infos">
      Aqui é onde vai ficar o cargo ou função profissional.
    </p>
  </div> <!-- .item -->
  </div>

  <div class="col-lg-4">
    <div class="item text-center">
      <img src="http://placehold.it/250x250" alt="" />
      <h2>Alessandra Ferreira</h2>

      <p class="infos">
        Aqui é onde vai ficar o cargo ou função profissional.
      </p>
    </div> <!-- .item -->
  </div>

</div>

</div> <!-- .container -->

</div> <!--#nossos-produtos -->

<?php require_once "footer.php"; ?>
