
  <footer id="topfooter">
    <div class="container">
      <div class="row">
        <div class="col-xs-6">
            <a href="#">
          <img src="images/rodape1.jpg" class="img-responsive" alt="" /></a>
        </div>
          <div class="col-xs-6">
            <a href="#">
            <img src="images/rodape2.jpg" class="img-responsive" alt="" /></a>
          </div>
      </div>
      <div class="row">
        <div class="agendamento-online col-xs-7">
          <h2>Faça seu agendamento on-line</h2>
          <p>
            Lorem ipsum dolor sit amet. <a href="#">Clique aqui</a> e faça já o agendamento do seu serviço online.
          </p>
        </div>
          <div class="app-agendamento-online col-xs-5">
            <h2>App Agenda Online</h2>
            <p>
              Baixe nosso aplicativo e agende seu horário pelo seu smartphone. <a href="#">Clique aqui</a>
            </p>
          </div>
      </div>
    </div>

  </footer>

  <footer id="mainfooter">
    <div class="container">
      <div class="row">

        <div class="col-xs-12 text-right" style="border-bottom: 1px solid rgba(255, 255, 255, 0.1);">

                    <p class="whatsapp">
                      <i class="icone fa fa-whatsapp"></i>
                      <strong>+55 73 88079776</strong>
                    </p>
                    <p class="telefone">
                      <i class="icone fa fa-phone"></i>
                      <strong>Ligue pra gente</strong>
                      <strong class="rosa">(73) 3525-6886</strong>
                    </p>
                    <div class="wrapper-redes-sociais">
                      <a href="https://facebook.com" target="_blank">
                        <i class="fa fa-facebook"></i>
                      </a>
                      <a href="https://instagram.com" target="_blank">
                        <i class="fa fa-instagram"></i>
                      </a>
                      <a href="https://youtube.com" target="_blank">
                        <i class="fa fa-youtube"></i>
                      </a>
                    </div> <!-- .wrapper-redes-sociais -->

        </div>
      </div>
      <div class="row lastline">
        <div class="col-xs-6">
          <p style="margin-top: 40px;">
            2015 Jocenando e Mary - Todos os direitos reservados
          </p>
        </div>
        <div class="col-xs-3">
          <i class="icone fa fa-map-marker" style="float: left; margin-top: 40px;"></i>
          <p style="float: left; margin-top: 35px;">
            Av. Rio Branco, 661, Centro <br>
            Jequié - BA CEP:45203-011
          </p>
        </div>
        <div class="col-xs-3 text-right">
          <a href="#" style="margin-top: 40px; display: inline-block;">
            <img src="images/autores.png" alt="" />
          </a>
        </div>
      </div>
    </div>
  </footer>
  <!-- jQuery -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>

  <!-- Bootstrap -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" charset="utf-8"></script>
</body>
</html>
