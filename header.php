<!DOCTYPE html>
<html lang="pt-BR" dir="ltr">
<head>
  <title>Jacenando & Mary</title>

  <meta name="viewport" content="width=device-width, initial-scale=1" />

  <!-- Google Fonts -->
  <link href='https://fonts.googleapis.com/css?family=PT+Sans:400,700' rel='stylesheet' type='text/css'>

  <!-- Bootstrap -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css">

  <!-- FontAwesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" media="screen" charset="utf-8">

  <!-- Application -->
  <link rel="stylesheet" href="style.css" media="screen" charset="utf-8">
  <link rel="stylesheet" href="media_queries.css" media="screen" charset="utf-8">
</head>
<body>

<header id="header">

  <div class="container">
  <div class="row">

    <div id="logo" class="col-lg-4 col-md-4 col-sm-6">
      <a href="index.php">
        <img src="images/logo.jpg" alt="Jacenando & Mary" />
      </a>
    </div> <!-- #logo -->

    <div id="nav-wrapper" class="col-lg-8 col-md-8 col-sm-6">

      <div class="row">
        <div class="col-lg-12 col-md-12 text-right pull-right">

          <p id="ligue-pra-gente">
            <i class="icone fa fa-phone"></i>
            Ligue pra gente <a href="tel:7335256886">(73) 3525-6886</a>
          </p> <!-- #ligue-pra-gente -->

          <div class="wrapper-redes-sociais">
            <a href="https://facebook.com" target="_blank">
              <i class="fa fa-facebook"></i>
            </a>
            <a href="https://instagram.com" target="_blank">
              <i class="fa fa-instagram"></i>
            </a>
            <a href="https://youtube.com" target="_blank">
              <i class="fa fa-youtube"></i>
            </a>
          </div> <!-- .wrapper-redes-sociais -->

        </div> <!-- .col-lg-12 -->
      </div> <!-- .row -->

      <div class="row">
        <div class="col-lg-12 col-md-12">

          <nav id="navbar">
            <a href="index.php">Home</a>
            <a href="page-sobre.php">Quem somos</a>
            <a href="page-servicos.php">Serviços</a>
            <a href="page-parceiros.php">Parceiros</a>
            <a href="page-produtos.php">Produtos</a>
            <a href="page-cadastre.php">Cadastre-se</a>
            <a href="page-contato.php">Contato</a>
          </nav> <!-- #navbar -->

        </div> <!-- .col-lg-12 -->
      </div> <!-- .row -->

    </div> <!-- #nav-wrapper -->

  </div> <!-- .row -->
</div> <!-- .container -->

</header> <!-- header -->
