<?php require_once "header.php"; ?>

<div class="pagetitle">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <h1>Serviços</h1>
      </div>
    </div>
  </div>
</div>

<div class="pagewrapper">
  <div class="container">
    <div class="row">
      <div class="col-xs-4">


          <ul id="cat-navbar">
            <li class="pai">
              <a href="#" class="pai-atual">Mulher <i class="caret"></i></a>

              <ul class="submenu">
                <li>
                  <a href="#" class="atual">Cabelo <i class="caret"></i></a>


                  <ul class="submenu">
                    <li>
                      <a href="#">Alongamento Capilar</a>
                    </li>
                    <li>
                      <a href="#" class="atual">Californiana / Balaiagem</a>
                    </li>
                      <li>
                        <a href="#">Alongamento Capilar</a>
                      </li>
                        <li>
                          <a href="#">Alongamento Capilar</a>
                        </li>
                          <li>
                            <a href="#">Alongamento Capilar</a>
                          </li>
                            <li>
                              <a href="#">Alongamento Capilar</a>
                            </li>
                              <li>
                                <a href="#">Alongamento Capilar</a>
                              </li>
                                <li>
                                  <a href="#">Alongamento Capilar</a>
                                </li>
                                  <li>
                                    <a href="#">Alongamento Capilar</a>
                                  </li>
                                    <li>
                                      <a href="#">Alongamento Capilar</a>
                                    </li>
                                      <li>
                                        <a href="#">Alongamento Capilar</a>
                                      </li>
                  </ul>
                </li>
              </ul>
            </li>
            <li class="pai"><a href="#">Homem <i class="caret"></i></a></li>
          </ul>

      </div>

      <div class="col-xs-8">

        <h2 class="pagetitle-with-border">Californiana / Balayagem</h2>

        <img src="http://placehold.it/750x300" class="img-responsive" alt="" />

        <div class="pagecontent">

          <p>
            Lorem ipsum dolor sit amet
          </p>
        </div>
      </div>
    </div>
  </div>
</div>

<?php require_once "footer.php"; ?>
