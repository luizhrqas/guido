<?php require_once "header.php"; ?>

<div class="pagetitle">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <h1>Produtos</h1>
      </div>
    </div>
  </div>
</div>

<div class="pagewrapper">
  <div class="container">
    <div class="row">
      <div class="col-xs-4">


          <ul id="cat-navbar">
            <li class="pai">
              <a href="#" class="pai-atual">Mulher <i class="caret"></i></a>

              <ul class="submenu">
                <li>
                  <a href="#" class="atual">Cabelo <i class="caret"></i></a>


                  <ul class="submenu">
                    <li>
                      <a href="#">Alongamento Capilar</a>
                    </li>
                    <li>
                      <a href="#" class="atual">Californiana / Balaiagem</a>
                    </li>
                      <li>
                        <a href="#">Alongamento Capilar</a>
                      </li>
                        <li>
                          <a href="#">Alongamento Capilar</a>
                        </li>
                          <li>
                            <a href="#">Alongamento Capilar</a>
                          </li>
                            <li>
                              <a href="#">Alongamento Capilar</a>
                            </li>
                              <li>
                                <a href="#">Alongamento Capilar</a>
                              </li>
                                <li>
                                  <a href="#">Alongamento Capilar</a>
                                </li>
                                  <li>
                                    <a href="#">Alongamento Capilar</a>
                                  </li>
                                    <li>
                                      <a href="#">Alongamento Capilar</a>
                                    </li>
                                      <li>
                                        <a href="#">Alongamento Capilar</a>
                                      </li>
                  </ul>
                </li>
              </ul>
            </li>
            <li class="pai"><a href="#">Homem <i class="caret"></i></a></li>
          </ul>

      </div>

      <div class="col-xs-8">

        <div class="migalhas">
          <p>
            Exibindo: <a href="#">Lanza Healing Hair Care</a> <span>></span> <a href="#">Healing Moisture</a>
          </p>
        </div>

        <div class="clearfix"></div>

        <div class="row">
        <div class="col-lg-6">

            <div class="item item-produto text-center">
              <img src="http://placehold.it/250x250" alt="" />
              <p>Essenza Profissional Kit Creme</p>
              <h2>R$7,98</h2>
              <div class="estrelas">
                <div class="estrela-dourada"></div>
                <div class="estrela-dourada"></div>
                <div class="estrela-dourada"></div>
                <div class="estrela-dourada"></div>
                <div class="estrela"></div>
              </div>
              <a href="single-produto.php">+ Detalhes</a>
              <span class="linha"></span>
            </div> <!-- .item item-produto -->

        </div>

        <div class="col-lg-6">

            <div class="item item-produto text-center">
              <img src="http://placehold.it/250x250" alt="" />
              <p>Essenza Profissional Kit Creme</p>
              <h2>R$7,98</h2>
              <div class="estrelas">
                <div class="estrela-dourada"></div>
                <div class="estrela-dourada"></div>
                <div class="estrela-dourada"></div>
                <div class="estrela-dourada"></div>
                <div class="estrela"></div>
              </div>
              <a href="single-produto.php">+ Detalhes</a>
              <span class="linha"></span>
            </div> <!-- .item item-produto -->

        </div>
        <div class="col-lg-6">

            <div class="item item-produto text-center">
              <img src="http://placehold.it/250x250" alt="" />
              <p>Essenza Profissional Kit Creme</p>
              <h2>R$7,98</h2>
              <div class="estrelas">
                <div class="estrela-dourada"></div>
                <div class="estrela-dourada"></div>
                <div class="estrela-dourada"></div>
                <div class="estrela-dourada"></div>
                <div class="estrela"></div>
              </div>
              <a href="single-produto.php">+ Detalhes</a>
              <span class="linha"></span>
            </div> <!-- .item item-produto -->

        </div>

        <div class="col-lg-6">

            <div class="item item-produto text-center">
              <img src="http://placehold.it/250x250" alt="" />
              <p>Essenza Profissional Kit Creme</p>
              <h2>R$7,98</h2>
              <div class="estrelas">
                <div class="estrela-dourada"></div>
                <div class="estrela-dourada"></div>
                <div class="estrela-dourada"></div>
                <div class="estrela-dourada"></div>
                <div class="estrela"></div>
              </div>
              <a href="single-produto.php">+ Detalhes</a>
              <span class="linha"></span>
            </div> <!-- .item item-produto -->

        </div>
        <div class="col-lg-6">

            <div class="item item-produto text-center">
              <img src="http://placehold.it/250x250" alt="" />
              <p>Essenza Profissional Kit Creme</p>
              <h2>R$7,98</h2>
              <div class="estrelas">
                <div class="estrela-dourada"></div>
                <div class="estrela-dourada"></div>
                <div class="estrela-dourada"></div>
                <div class="estrela-dourada"></div>
                <div class="estrela"></div>
              </div>
              <a href="single-produto.php">+ Detalhes</a>
              <span class="linha"></span>
            </div> <!-- .item item-produto -->

        </div>

        <div class="col-lg-6">

            <div class="item item-produto text-center">
              <img src="http://placehold.it/250x250" alt="" />
              <p>Essenza Profissional Kit Creme</p>
              <h2>R$7,98</h2>
              <div class="estrelas">
                <div class="estrela-dourada"></div>
                <div class="estrela-dourada"></div>
                <div class="estrela-dourada"></div>
                <div class="estrela-dourada"></div>
                <div class="estrela"></div>
              </div>
              <a href="single-produto.php">+ Detalhes</a>
              <span class="linha"></span>
            </div> <!-- .item item-produto -->

        </div>

        </div>

      </div>
    </div>
  </div>
</div>

<?php require_once "footer.php"; ?>
